import { useEffect, useState } from 'react';

const useImagePreloader = (imageSources) => {
  const [imagesLoaded, setImagesLoaded] = useState(false);

  useEffect(() => {
    const preloadImages = imageSources.map(imageSrc => {
      const image = new Image();
      image.src = imageSrc;
      return image;
    });

    Promise.all(preloadImages.map(image => {
      return new Promise(resolve => {
        image.onload = resolve;
      });
    })).then(() => {
      setImagesLoaded(true);
    });

    return () => {
      preloadImages.forEach(image => {
        image.onload = null;
      });
    };
  }, [imageSources]);

  return imagesLoaded;
};

export default useImagePreloader;