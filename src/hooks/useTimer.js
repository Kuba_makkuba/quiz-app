/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from "react";
import { useDataContext } from "../context";

const useTimer = () => {

  const { state, setState, setItems } = useDataContext();

  useEffect(() => {

    let interval = null;
    if (state.time) {
      interval = setInterval(() => {
        setState({...state, seconds: state.seconds + 1})
      }, 1000);
    } else if (!state.time && state.seconds !== 0) {
      clearInterval(interval);
    }

    if (state.seconds === 3) {
      setState({...state, 
        seconds: state.seconds = state.seconds = 0, 
        time: (state.time = false), 
        round: state.round = state.round + 1, 
        indexItem: (state.indexItem = null)
      },);

      if (state.round % 4 === 0) {
        setState({ ...state, imageIndex: (state.imageIndex = 0)});
      } else {
        setState({ ...state, imageIndex: (state.imageIndex = state.imageIndex + 1)});
      }
      setItems([])
    }

    return () => clearInterval(interval);

  },[state])
};


export default useTimer;
