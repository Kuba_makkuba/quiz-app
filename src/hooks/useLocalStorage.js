/* eslint-disable no-undef */
import { useEffect, useState } from "react";

const useLocalStorage = () => {
  
  const [items, setItems] = useState([]);
 
  useEffect(() => {
    if (localStorage.dnd !== undefined) {
      setItems(JSON.parse(localStorage.dnd));
    }
  }, []);

  const [state, setState] = useState({
    imageIndex: Number(window.localStorage.getItem("imageIndex") || 0),
    round: Number(window.localStorage.getItem("round") || 0),
    time: Boolean(window.localStorage.getItem("time") === "true"),
    point: Number(window.localStorage.getItem("point") || 0),
    seconds: Number(window.localStorage.getItem("seconds") || 0),
    indexItem: Number(window.localStorage.getItem("indexItem") || null),
    isGoodAnswer: Boolean(window.localStorage.getItem("isGoodAnswer") === "true"
    ),
  });

  useEffect(() => {
    window.localStorage.setItem("imageIndex", state.imageIndex);
    window.localStorage.setItem("round", state.round);
    window.localStorage.setItem("seconds", state.seconds);
    window.localStorage.setItem("time", state.time);
    window.localStorage.setItem("point", state.point);
    window.localStorage.setItem("indexItem", state.indexItem);
    window.localStorage.setItem("isGoodAnswer", state.isGoodAnswer);
  });

  return { state, setState, items, setItems };
};

export default useLocalStorage;
