import React from "react";
import ReactDOM from "react-dom";
import { APIContextProvider } from "./context";
import { RouterProvider } from "react-router-dom";
import routes from "./routes/index";
import { HelmetProvider } from "react-helmet-async";
import Head from "./components/head";
import "./styles/themes/default/theme.scss";

ReactDOM.render(
  <HelmetProvider>
    <Head />
    <APIContextProvider>
      <RouterProvider router={routes} />
    </APIContextProvider>
  </HelmetProvider>,
  document.getElementById("root")
);
