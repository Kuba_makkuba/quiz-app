import { imports } from "../../imports/index";

const Questions = (item) => {

  const { Loading, Answers, useDataContext } = imports;

  const { state, dataBase } = useDataContext();
  const data = dataBase[state.round];

  return (
    <div className="questions">
        {item.load && data !== undefined ?
        <div className="questions__wrapper">
          <p className="questions__text">{data.question}</p>
          <Answers {...item}/>
        </div> : <Loading full={false}/>}
    </div>
  );
};

export default Questions;
