import { imports } from "../../imports/index";

const Score = (props) => {

    const { Link, useDataContext, arrow } = imports;

    const { setState, state } = useDataContext(); 
    const { imagecategory, category, button, shadow, item, link } = props.item;

    const replay = () => setState({...state, round: 0, point: 0, imageIndex: 0});

    return (
        <div className="score">
            <img className="score__image" src={imagecategory} alt={category}/>
            <h2 className="score__name">{category}</h2>
            <div className="score__wrapper">
                <div className={`score__box ${shadow}`}>
                    <p>TWÓJ WYNIK</p>
                </div>
                <div className={`score__circle ${item}`}>
                    <p>{state.point}/10</p>
                </div>
            </div>
            <Link to={link}>
                <button className={`score__button ${button}`} onClick={replay}>POWTÓRZ QUIZ
                    <img className="score__arrow" src={arrow} alt="arrow" />
                </button>
            </Link>
        </div>
    )
}

export default Score;