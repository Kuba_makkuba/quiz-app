/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from "react";
import { imports} from "../../imports";

export const DnD = ({ item }) => {

  const { DndProvider, Column, Movable, classNames, MyPreview, HTML5Backend, BrowserView, MobileView, TouchBackend, useDataContext, useTimer, goodAnswer, badAnswer } = imports;

  const { dataBase, state, items, setItems } = useDataContext();
  const data = dataBase[state.round].database;
  const correctAnswer = dataBase[state.round].comparison;
  const props = item.button;

  const COLUMN_NAMES = {
    DO_IT: "Do it",
    IN_PROGRESS: "In Progress",
  };

  const { DO_IT, IN_PROGRESS } = COLUMN_NAMES;

  useTimer();

  const newArr = Array.from(Array(data.length).keys());

  var answer = newArr.map((id, index) => {
    return {
      id: index + 1,
      name: data[index],
      column: DO_IT,
    };
  });

  useEffect(() => {
    if (items.length === 0) {
      setItems(answer);
    }
  }, []);

  localStorage.setItem("dnd", JSON.stringify(items));

  const returnItemsForColumn = (columnName, props) => {
    return items
      .filter((item) => item.column === columnName)
      .map((item, index) => (
        <Movable
          item={props}
          key={item.id}
          name={item.name}
          currentColumnName={item.column}
          setItems={setItems}
          index={index}
          items={items}
          correctAnswer={correctAnswer}
          COLUMN_NAMES={COLUMN_NAMES}
        />
      ));
  };

  const isImg = state.isGoodAnswer ? goodAnswer : badAnswer;
  const effect = state.seconds > 1 ? "dnd__imageTwo" : "dnd__imageOne";

  return (
    <>
      <BrowserView>
        <DndProvider backend={HTML5Backend}>
          <Column
            title={IN_PROGRESS}
            className={classNames("column column__progress", props)}
            COLUMN_NAMES={COLUMN_NAMES}
          >
            {state.time ? (
              <img className={effect} src={isImg} alt="answer" />
            ) : null}
            {returnItemsForColumn(IN_PROGRESS, props)}
          </Column>
          <Column title={DO_IT} className="column column__doIt" COLUMN_NAMES={COLUMN_NAMES}>
            {returnItemsForColumn(DO_IT, props)}
          </Column>
        </DndProvider>
      </BrowserView>
      <MobileView>
        <DndProvider backend={TouchBackend}>
          <Column
            title={IN_PROGRESS}
            className={classNames("column column__progress", props)}
            COLUMN_NAMES={COLUMN_NAMES}
          >
            {state.time ? (<img className={effect} src={isImg} alt="answer" />) : null}
            {returnItemsForColumn(IN_PROGRESS, props)}
          </Column>
          <Column title={DO_IT} className="column column__doIt" COLUMN_NAMES={COLUMN_NAMES}>
            {returnItemsForColumn(DO_IT, props)}
          </Column>
          <MyPreview background={props} />
        </DndProvider>
      </MobileView>
    </>
  );
};