import { imports } from "../../imports";
import { useRef } from "react";

const Movable = ({name, index, currentColumnName, setItems, item, correctAnswer, COLUMN_NAMES}) => {

  const { classNames, useDataContext, useDrag, useDrop } = imports;  
  const { state, setState } = useDataContext();

  const changeItemColumn = (currentItem, columnName) => {
    setItems((prevState) => {
      return prevState.map((e) => {
        return {
          ...e,
          column: e.name === currentItem.name ? columnName : e.column,
        };
      });
    });
  };

  const ref = useRef(null);

  const [, drop] = useDrop({
    accept: "Our first type",
    hover(item, monitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;

      if (dragIndex === hoverIndex) {
        return;
      }

      const hoverBoundingRect = ref.current?.getBoundingClientRect();

      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

      const clientOffset = monitor.getClientOffset();
      const hoverClientY = clientOffset.y - hoverBoundingRect.top;

      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }

      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }

      item.index = hoverIndex;
    },
  });

  const [, drag] = useDrag({
    canDrag: !state.time,
    type: "Our first type",
    item: { index, name, currentColumnName, type: "Our first type" },
    end: (item, monitor) => {
      const dropResult = monitor.getDropResult();

      if (dropResult) {
        const { name } = dropResult;
        const { DO_IT, IN_PROGRESS } = COLUMN_NAMES;

        if (dropResult.name === "In Progress") {
          setState({
            ...state,
            time: (state.time = true),
            indexItem: (state.indexItem = index),
          });
          if (item.name === correctAnswer) {
            setState({
              ...state,
              point: (state.point = state.point + 1),
              isGoodAnswer: (state.isGoodAnswer = true),
            });
          } else {
            setState({ ...state, isGoodAnswer: (state.isGoodAnswer = false) });
          }
        }

        switch (name) {
          case IN_PROGRESS:
            changeItemColumn(item, IN_PROGRESS);
            break;
          case DO_IT:
            changeItemColumn(item, DO_IT);
            break;
          default:
            break;
        }
      }
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  drag(drop(ref));

  const opacity = state.time ? "movable__itemTwo" : "movable__item";

  return (
    <div ref={ref} className={classNames(opacity, item)}>
      <p className="movable__text">{name}</p>
    </div>
  );
};

export default Movable;
