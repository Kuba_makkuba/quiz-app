import { imports } from "../../imports/index";

const MyPreview = ({ background }) => {
    
  const { usePreview } = imports;

  const preview = usePreview();
  if (!preview.display) {
    return null;
  }

  const { item, style } = preview;

  return (
    <div className={`${background} movable__itemTwo`} style={style}>
      <p className="movable__text">{item.name}</p>
    </div>
  );
};

export default MyPreview;
