import { imports } from "../../imports";

const Head = () => {

  const { Helmet } = imports;
  
  return (
      <Helmet>
        <link rel="canonical" href="https://quizapp-jm.netlify.app"/>
        <link rel="manifest" href="/manifest.json" />
        <link href="https://fonts.googleapis.com/css2?family=Rakkas&display=swap" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css2?family=Nunito&display=swap" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css2?family=Gluten:wght@200&display=swap" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css2?family=Kosugi&display=swap" rel="stylesheet"/>
        <meta name="description" content="Quiz - try your best and answer all the questions correctly" />
        <meta name="referrer" content='strict-origin'/>
        <meta name="theme-color" content="#4a0d6d" />
        <meta name="Content-Type" content="text/html; charset=utf-8" />
        <meta name="referrer" content="strict-origin" />
        <meta httpEquiv="Content-Security-Policy" />
        <meta name="author" content="Jakub Makowski" />
        <meta name="keywords" content="Quiz, project, frontend" />
        <meta name="robots" content="index, follow" />
        <meta charSet="UTF-8" http-equiv="X-UA-Compatible" name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover"/>
        <meta property="og:title" content="Quiz" />
        <meta property="og:description" content="Quiz - try your best and answer all the questions correctly" />
        <title>Quiz</title>
      </Helmet>
  );
}

export default Head;