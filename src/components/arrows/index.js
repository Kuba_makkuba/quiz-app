import { imports } from "../../imports";

const Arrows = () => {

    const { arrow } = imports;

    return(
        <div className="arrows">
            <div className="prev">
            <button aria-label="arrow left" className="arrows__buttonPrev">
                <img src={arrow} alt="arrow-left" className="arrows__imageLeft"/>
            </button>
          </div>
          <div className="next">
            <button aria-label="arrow right" className="arrows__buttonNext">
                <img src={arrow} alt="arrow-right" className="arrows__imageRight"/>
            </button>
          </div>
        </div>
    )
}

export default Arrows;