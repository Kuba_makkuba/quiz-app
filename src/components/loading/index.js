const Loading = ({full}) => {

  const isFull = full ? "loaderFull" : "loader";

  return (
    <div className={isFull}>
      <div className="lds-roller">
        <div/>
        <div/>
        <div/>
        <div/>
        <div/>
        <div/>
        <div/>
        <div/>
      </div>
    </div>
  );
};

export default Loading;
