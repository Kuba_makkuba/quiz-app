import { imports } from "../../imports/index";

const Menu = () => {
  
  const { Container } = imports;

  return <Container name="10 PYTAŃ / 5 KATEGORII" />;
};

export default Menu;