import { imports } from "../../imports/index";

const Column = ({ children, className, title, COLUMN_NAMES }) => {

  const { useDrop } = imports;

  const [, drop] = useDrop({
    accept: "Our first type",
    drop: () => ({ name: title }),
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),

    canDrop: (item) => {
      const { DO_IT, IN_PROGRESS } = COLUMN_NAMES;
      const { currentColumnName } = item;

      return (
        currentColumnName === title ||
        (currentColumnName === DO_IT && title === IN_PROGRESS) ||
        (currentColumnName === IN_PROGRESS && title === DO_IT) ||
        title === IN_PROGRESS
      );
    },
  });

  return (
    <div className={className} ref={drop}>
      {children}
    </div>
  );
};

export default Column;