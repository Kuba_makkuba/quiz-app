import { imports } from "../../imports/index";
import classNames from "classnames";

const Start = ({ item }) => {

  const { Link, useDataContext, arrow } = imports;

  const { button, imagecategory, category, link2 } = item;
  const { setState, state } = useDataContext();
  
  const beginning = () => {
    setState({...state, 
      round: (state.round = 0), 
      point: (state.point = 0), 
      seconds: state.seconds = state.seconds = 0, 
      time: (state.time = false),
      imageIndex: (state.imageIndex = 0)
    });
  } 
  
  return (
    <div className="start">
      <img className="start__icon" src={imagecategory} alt="category" />
      <h2 className="start__title">{category}</h2>
      <Link to={link2}>
        <button onClick={beginning} className={classNames("start__button", button)}>
          ROZPOCZNIJ
          <img className="start__arrow" src={arrow} alt="arrow" />
        </button>
      </Link>
    </div>
  );
};

export default Start;
