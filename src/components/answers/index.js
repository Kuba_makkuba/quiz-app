import { imports } from "../../imports/index";

const Answers = ({ item }) => {

  const { badAnswer, goodAnswer, DnD, useDataContext } = imports;
  
  const { dataBase, state, setState } = useDataContext();
  const { comparison, database } = dataBase[state.round];

  const selectAnswer = (answer, goodAnswer, index) => {

    setState({ ...state, time: (state.time = true),  indexItem : (state.indexItem = index)});
    
    if (answer === goodAnswer) {
      setState({ ...state, 
        point: (state.point = state.point + 1), 
        isGoodAnswer: (state.isGoodAnswer = true)
      });
    }
    else {
      setState({ ...state, 
        isGoodAnswer: (state.isGoodAnswer = false)
      });
    }
  };

  const imgAnswer = state.isGoodAnswer ? goodAnswer : badAnswer;
  const effect = state.seconds > 1 ? "answers__imageTwo" : "answers__imageOne";

  const classVerificationImg = (index) => {
    return state.indexItem === index ? <img className={effect} src={imgAnswer} alt="answer"/> : null;
  }

  const classVerificationButton = () => {
    return `${item.button} ${!state.time ? "answers__button" : "button__notActive"}`;
}

  return (
    <div className="answers">
      {state.round % 2 === 0 ? (
        database.map((item, index) => (
          <div className="answers__box" key={index}>
            <button
              disabled={state.time}
              onClick={() => selectAnswer(item, comparison, index)}
              key={index}
              className={classVerificationButton()}
              >
              {item}
            </button>
            {classVerificationImg(index)}
          </div>
        ))
      ) : (
        <DnD {...{item}}/>
      )}
    </div>
  );
};

export default Answers;
