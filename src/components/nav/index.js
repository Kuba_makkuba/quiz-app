import { imports } from "../../imports/index";
import "swiper/swiper.min.css";
import "swiper/swiper.min.css";

const Nav = ({ item }) => {

  const { classNames, resourcesData, React, Link, SwiperSlide, Swiper, Arrows, Navigation, useDataContext, useLocation, useMediaQuery } = imports;
  
  const { setState, state, setDataBase, setLoad } = useDataContext();
  const location = useLocation();

  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-width: 1001px)'
  })

  const filter = resourcesData.filter(name => {
    return name.category !== item.category
  })

  const choose = () => {
    setLoad(false)
    setDataBase([])
    setState({...state, 
      round: 0, 
      point: 0, 
      seconds: 0, 
      time: false, 
      indexItem: null,
      imageIndex: 0,
    });
  }

  const menu = item.length ? "navMain" : "nav"
  const itemClass = location.pathname !== "/" ? "nav__item" : "nav__itemTwo"

  const links = (name) => {

    return (
      <Link to={name.link} t>
        <li className={classNames(itemClass, item.button)} onClick={choose}>
          <img className="nav__image" src={name.imagecategory} alt={name.category}/>
          <p className="nav__name">{name.category}</p>
        </li>
      </Link>
    )
  }

  const sliderCategories = filter.map((name, index) => (
    <SwiperSlide key={index}>
      {links(name)}
    </SwiperSlide>
  ));

  const categories = filter.map((name, index) => (
      <div className="nav__link" key={index}>
        {links(name)}
      </div>
  ));

  return (
    <div className={menu}>
      {!item.length ? <h3 className="nav__title">WYBIERZ KATEGORIE</h3> : null}
      <nav className="nav__wrapper">
        <ul className="nav__menu">
        {!item.length ? 
        <div className="nav__slider">
          {isDesktopOrLaptop ? categories : 
            <Swiper 
              modules={[Navigation]} 
              navigation={{prevEl: ".prev", nextEl: ".next"}} 
              loop={true}>{sliderCategories}
            </Swiper>}
          <div >
          <Arrows />
        </div>
        </div> : 
        <div className="nav__sliderOff">{categories}</div>}
        </ul>
      </nav>
    </div>
  );
};

export default Nav;
