import React from 'react';
import { imports } from "../../imports/index";

const Container = ({ name, children, item, backPage }) => {

  const { classNames, Nav, Link , q, exit, back, useDataContext, useImagePreloader, resourcesData } = imports;

  const { state } = useDataContext();

  const allImages = resourcesData.reduce((acc, data) => [...acc, ...data.backgroundAll], []);
  const imagesLoaded = useImagePreloader(allImages);

  const isItem = (elementOne, elementTwo) => {
    return !item ? elementOne : elementTwo;
  };

  const image = !item ? allImages[12] : item.backgroundAll[state.imageIndex];
  const shadow = !item ? "shadow__programming" : item.shadow;

  return (
    <div className="container">
      {imagesLoaded ? (
        <>
          {item !== undefined &&
            item.category === "PROGRAMOWANIE" &&
            state.round === 10 ? (
              <img className="container__image" src={allImages[15]} alt="programmingImg" />
            ) : (
              <img className="container__image" src={image} alt="bg"/>
            )}
          
          <div className="container__main">
            <img className="container__logo" src={q} alt="q" />
            <div className="container__wrapper">
              <h1 className="container__title">Quiz</h1>
              <Link to={backPage === undefined ? "/" : backPage}>
                <button className="container__buttonBack">
                  <img className="container__icon" src={back} alt="back" />
                </button>
              </Link>
              <Link to="/">
                <button className="container__buttonMenu">
                  <img className="container__icon" src={exit} alt="menu" />
                </button>
              </Link>
            </div>
            <div className={classNames("container__gradient", isItem("shadow__technology", shadow))}>
              <h2>{name}</h2>
            </div>
            {children}
            <Nav item={isItem(resourcesData, item)} />
          </div>
        </>
      ) : (
        null
      )}
    </div>
  );
};

export default Container;