import React from "react";

import Menu from "../components/menu";
import QuizSelected from "../pages/quizSelected";
import Quiz from "../pages/quiz";

import { createBrowserRouter } from "react-router-dom";

const routesData = [
    { path: "/", component: Menu },
    { path: "/technology", component: QuizSelected, index: 0 },
    { path: "/culture", component: QuizSelected, index: 1 },
    { path: "/automotive", component: QuizSelected, index: 2 },
    { path: "/programming", component: QuizSelected, index: 3 },
    { path: "/history", component: QuizSelected, index: 4 },
    { path: "/technology/quiz", component: Quiz, index: 0 },
    { path: "/culture/quiz", component: Quiz, index: 1 },
    { path: "/automotive/quiz", component: Quiz, index: 2 },
    { path: "/programming/quiz", component: Quiz, index: 3 },
    { path: "/history/quiz", component: Quiz, index: 4 },
  ];
  
  const routes = createBrowserRouter(
    routesData.map(({ path, component, index }) => ({
      path,
      element: React.createElement(component, { index }),
    }))
  );
  
  export default routes;