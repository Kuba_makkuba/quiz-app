import { useEffect } from 'react';

const useFetchData = (location, newString, apiKey, setDataBase, setLoad) => {
  
  useEffect(() => {
    if (location.pathname !== `${newString}/quiz`) {
      return;
    }

    fetch(`${apiKey}${location.pathname}.json`)
      .then((response) => response.json())
      .then((json) => {
        setDataBase(json);
        const timer = setTimeout(() => {
          setLoad(true);
        }, 1000);
        return () => clearTimeout(timer);
      });
  }, [location.pathname, newString, apiKey, setDataBase, setLoad]);
};

export default useFetchData;