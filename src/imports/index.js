import { BrowserRouter as Router, Route } from "react-router-dom";
import { DndProvider, useDrag, useDrop } from "react-dnd";
import { SwiperSlide, Swiper } from "swiper/react";
import { FreeMode, Thumbs, EffectFade, Navigation } from "swiper";
import { BrowserView, MobileView } from "react-device-detect";
import { HTML5Backend } from "react-dnd-html5-backend";
import { TouchBackend } from "react-dnd-touch-backend";

import Answers from "../components/answers";
import Arrows from "../components/arrows";
import Box from "../components/start";
import classNames from "classnames";
import Column from "../components/column";
import Container from "../components/container";
import { Helmet } from 'react-helmet-async';
import Loading from "../components/loading";
import Menu from "../components/menu";
import Movable from "../components/movable";
import MyPreview from "../components/myPreview";
import Nav from "../components/nav/index";
import Questions from "../components/questions";
import Score from "../components/score";
import { DnD } from "../components/dnd";
import { Link } from "react-router-dom";
import Quiz from "../pages/quiz";
import QuizSelected from "../pages/quizSelected";

import arrow from "../assets/others/cofnij_x.png";
import automotiveIcon from "../assets/others/motoryzacja_ikona.png";
import back from "../assets/others/cofnij_x.png";
import badAnswer from "../assets/others/badAnswer.png";
import cultureIcon from "../assets/others/kultura_ikona.png";
import exit from "../assets/others/zamknij_x.png";
import goodAnswer from "../assets/others/check-mark.png";
import historyIcon from "../assets/others/historia_ikona.png";
import programmingIcon from "../assets/others/programowanie_ikona.png";
import q from "../assets/others/q.png";
import technologyIcon from "../assets/others/technologia_ikona_.png";

import "../assets/auto/motoryzacja_tlo.webp";
import "../assets/auto/motoryzacja_tlo2.webp";
import "../assets/auto/motoryzacja_tlo3.webp";
import "../assets/programmer/programowanie_tlo.webp";
import "../assets/programmer/programowanie_tlo3.webp";
import "../assets/programmer/programowanie_tlo30.webp";
import "../assets/programmer/programowanie_tlo5.webp";
import "../assets/culture/kultura_tlo.webp";
import "../assets/culture/kultura_tlo2.webp";
import "../assets/culture/kultura_tlo3.webp";
import "../assets/culture/kultura_tlo4.webp";
import "../assets/technology/technologia_tlo.webp";
import "../assets/technology/technologia_tlo2.webp";
import "../assets/history/historia_tlo.webp";
import "../assets/history/historia_tlo2.webp";
import "../assets/history/historia_tlo22.webp";

import { useDataContext } from "../context";
import { useLocation } from "react-router-dom";
import { useMediaQuery } from 'react-responsive';
import { usePreview } from "react-dnd-preview";
import useFetchData from "../api/useFetchData";
import useImagePreloader from "../hooks/useImagePreloader";
import useLocalStorage from "../hooks/useLocalStorage";
import useTimer from "../hooks/useTimer";

import resourcesData from "../resources/resources.json";

export const imports = {
  Answers,
  Arrows,
  Box,
  BrowserView,
  classNames,
  Column,
  Container,
  DnD,
  DndProvider,
  EffectFade,
  FreeMode,
  HTML5Backend,
  Helmet,
  Link,
  Loading,
  Menu,
  MobileView,
  Movable,
  MyPreview,
  Navigation,
  Nav,
  Questions,
  Quiz,
  QuizSelected,
  Route,
  Router,
  Score,
  Swiper,
  SwiperSlide,
  Thumbs,
  TouchBackend,
  arrow,
  automotiveIcon,
  back,
  badAnswer,
  cultureIcon,
  exit,
  goodAnswer,
  historyIcon,
  programmingIcon,
  q,
  technologyIcon,
  useDataContext,
  useFetchData,
  useImagePreloader,
  useLocalStorage,
  useLocation,
  useMediaQuery,
  usePreview,
  useTimer,
  useDrag, 
  useDrop,
  resourcesData
};