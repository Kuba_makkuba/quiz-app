import { imports } from "../../imports/index";

const QuizSelected = ({index}) => {

  const { resourcesData, Container, Box } = imports;
  const item = resourcesData[index];

  return (
      <Container name="WYBRANA KATEGORIA" item={item} backPage="/">
        <Box {...{item}}/>
      </Container>
  );
};

export default QuizSelected;