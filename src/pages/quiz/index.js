/* eslint-disable react-hooks/exhaustive-deps */
import { imports } from "../../imports/index";

const Quiz = ({ index }) => {

  const { resourcesData, Container, Questions, Score, useDataContext, useLocation, useTimer, useFetchData } = imports;

  const apiKey = process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_KEY_PROD : process.env.REACT_APP_API_KEY_LOCAL;
  const item = resourcesData[index];
  const location = useLocation();

  const { state, setDataBase, load, setLoad } = useDataContext();

  const newString = location.pathname.replace("/quiz", "");

  useTimer();
  useFetchData(location, newString, apiKey, setDataBase, setLoad);

  const name = state.round % 2 === 0 ? "SELECT THE CORRECT ANSWER" : "DRAG & DROP THE RIGHT ANSWER";

  return (
    <Container name={name} item={item} backPage={item.link}>
      {state.round === 10 ? (
        <Score item={item} />
      ) : (
        <Questions {...{ item, load }} />
      )}
    </Container>
  );
};

export default Quiz;