import React, { useContext, createContext, useState, useMemo } from "react";
import { imports } from "../imports";

const APIContext = createContext({
  dataBase: [],
  setDataBase: () => {},
  load: false,
  setLoad: () => {},
  state: { round: 0, time: false, point: 0, seconds: 0, indexItem: 0, isGoodAnswer: false },
  setState: () => {},
  items: [],
  setItems: () => {},
  isLoadImages: false, 
  setIsLoadImage: () => {}
});

export function APIContextProvider({ children }) {
  
  const { useLocalStorage } = imports;
  const { setState, state, items, setItems } = useLocalStorage();

  const [dataBase, setDataBase] = useState([]);
  const [load, setLoad] = useState(false);
  const [isLoadImages, setIsLoadImage] = useState(false);

  const contextValue = useMemo(() => ({
    dataBase,
    setDataBase,
    load,
    setLoad,
    state,
    setState,
    items,
    setItems,
    isLoadImages, 
    setIsLoadImage
  }), [dataBase, load, state, items, setItems, setState, isLoadImages, setIsLoadImage]);

  return (
    <APIContext.Provider value={contextValue}>
      {children}
    </APIContext.Provider>
  );
}

export function useDataContext() {
  const context = useContext(APIContext);
  return context;
}
